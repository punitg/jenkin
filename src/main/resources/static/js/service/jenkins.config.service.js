var app = angular.module("ConfigApp",[]);


app.service("ConfigService", ["$http", function($http) {
	
	var self = this;
	
    self.getAllConfig = function() {
        return $http.get("/rest/config");
    }
    
    self.getConfig = function(ALMProjectKey){
    	return $http.get("/rest/config/" + ALMProjectKey);
    }
    
    self.removeConfig = function(ALMProjectKey){
    	return $http.delete("/rest/config/" + ALMProjectKey);
    }
    
    self.addConfig = function(newConfig){
        return $http.post("/rest/config", newConfig);
    }
    
    self.updateConfig = function(tempConfig){
        return $http.put("/rest/config/" + tempConfig.ALMProjectKey, tempConfig);
    }
    
    self.getAllALMProjectKeys = function() {
        return $http.get("/rest/config/keys");
    }
    
}]);