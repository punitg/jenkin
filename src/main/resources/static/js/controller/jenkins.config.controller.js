var app = angular.module("ConfigApp");

app.controller("ConfigController", ["ConfigService", function (ConfigService) {

    var self = this;

    function getAllALMProjectKeys() {
        ConfigService.getAllALMProjectKeys().then(function (response) {
            self.allALMProjectKeys = response.data;
            // console.log(self.allALMProjectKeys);
        });
    }

    function init() {
        getAllALMProjectKeys();
    };

    init();

    self.loadConfigForEdit = function (ALMProjectKey,callback) {
        self.opMode = "edit";
        self.newJob = "";
        ConfigService.getConfig(ALMProjectKey).then(function (response) {
            self.tempConfig = response.data;
            // console.log(self.tempConfig);
            callback();
        });
    }

    self.loadConfigForAdd = function (callback) {
        self.opMode = "add";
        self.newJob = "";
        self.tempConfig = {
            "ALMProjectKey": "",
            "JobList": {
                "OtherJobs": [{
                    "FolderName": "",
                    "JobNames": []
                }
                ],
                "CIJobs": [{
                    "FolderName": "",
                    "JobNames": []
                }
                ]
            }
        }
        callback();
        // console.log("mode : " + self.opMode);
    }

    function clearMode() {
        self.opMode = undefined;
        self.tempConfig = undefined;
        self.isJobListEmpty= undefined;
    }

    $('#editProjectModal').on('hidden.bs.modal', function () {
        clearMode();
    });

    self.addJob = function () {
        if (self.newJob.trim().length > 0) {
            if (self.tempConfig.JobList.OtherJobs[0].JobNames == null) {
                self.tempConfig.JobList.OtherJobs[0].JobNames = [];
            }
            if (!self.tempConfig.JobList.OtherJobs[0].JobNames.includes(self.newJob)) {
                self.tempConfig.JobList.OtherJobs[0].JobNames.push(self.newJob);
            }
            self.newJob = "";
        }
        self.checkForEmptyJobList();
    }

    self.removeJob = function (index) {
        self.tempConfig.JobList.OtherJobs[0].JobNames.splice(index, 1);
        self.checkForEmptyJobList();
    }

    self.addConfig = function () {
        ConfigService.addConfig(self.tempConfig).then(function (response) {
            // console.log(response.data);
            getAllALMProjectKeys();
        });
    }

    self.updateConfig = function () {
        ConfigService.updateConfig(self.tempConfig).then(function (response) {
            // console.log(response.data);
            getAllALMProjectKeys();
        });
    }

    self.removeConfig = function (ALMProjectKey) {
        ConfigService.removeConfig(ALMProjectKey).then(function (response) {
            // console.log(response.data);
            getAllALMProjectKeys();
        });
    }

    self.save = function () {
        if (self.opMode == 'add') {
            self.addConfig();
        } else if (self.opMode == 'edit') {
            self.updateConfig();
        }
    }

    self.checkALMProjectKeyAvailability = function () {
        if (self.allALMProjectKeys.includes(self.tempConfig.ALMProjectKey)) {
            self.isALMProjectKeyAvailable = false;
        } else {
            self.isALMProjectKeyAvailable = true;
        }
    }

    self.checkForEmptyJobList = function () {
        // console.log("length : " + self.tempConfig.JobList.OtherJobs[0].JobNames.length);
        if (self.tempConfig.JobList.OtherJobs[0].JobNames.length == 0) {
            self.isJobListEmpty=true;
        } else {
            self.isJobListEmpty=false;
        }
    }

}]);
