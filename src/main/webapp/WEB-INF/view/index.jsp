<html ng-app="ConfigApp">

<head>
    <title>Jenkins Configuration</title>+

    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/main.css" />
    <script src="js/angular.min.js"></script>
</head>

<body ng-controller="ConfigController as config">
    <div class="container">
        <div class="page-header">
            <h1 class="text-center">Jenkins Jobs Configuration</h1>
        </div>

        <div class="row">
            <div class="col-xs-12" style="height:15px;"></div>
        </div>

        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-8">
                <input type="text" class="form-control input-lg" placeholder="Search for project" ng-model="searchALMProjectKey">
            </div>

            <div class="col-lg-2">
                <button type="button" class="btn btn-lg btn-success pull-right" data-toggle="modal" data-target="#editProjectModal" ng-click="config.loadConfigForAdd(config.checkForEmptyJobList)">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp; Add Project &nbsp;
                </button>
            </div>

            <div class="row">
                <div class="col-xs-12" style="height:15px;"></div>
            </div>

            <hr class="soften">

            <div class="row">
                <div class="col-xs-12" style="height:15px;"></div>
            </div>

            <div class="row">
                <div class="col-lg-1"></div>
                <div class="container" style="width: 95%;">
                    <div class="list-group" style="max-height: 500px;overflow: auto;">
                        <a href="#" ng-repeat="aLMProjectKey in config.allALMProjectKeys | filter:searchALMProjectKey" class="list-group-item">
                            <div class="row">
                                <div class="col-lg-8 col-md-6 col-sm-4">
                                    <h5 style="font-size:18px">{{aLMProjectKey}}</h5>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-4">
                                    <button class="btn btn-default btn-lg btn-warning pull-right" type="button" data-toggle="modal" ng-click="config.loadConfigForEdit(aLMProjectKey,config.checkForEmptyJobList)"
                                        data-target="#editProjectModal">
                                        <span class="glyphicon glyphicon-pencil" style="margin-right: 5px;"></span>
                                        Edit
                                    </button>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-4">
                                    <button class="btn btn-default btn-lg btn-danger" type="button" confirmed-click="config.removeConfig(aLMProjectKey)" ng-confirm-click="Are you sure, you want to delete project configuration?">
                                        <span class="glyphicon glyphicon-trash" style="margin-right: 5px;"></span>
                                        Delete
                                    </button>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="editProjectModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 ng-show="config.opMode=='add'" class="modal-title">Add Project Configuration</h4>
                            <h4 ng-show="config.opMode=='edit'" class="modal-title">Edit Project Configuration</h4>
                        </div>
                        <form name="configForm" novalidate>
                            <div class="modal-body form-group">
                                <div class="row">
                                    <label class="control-label col-lg-3 text-right">ALM Project Key</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="ALM Project Key (Required)" ng-model="config.tempConfig.ALMProjectKey"
                                            id="aLMProjectKey" ng-readonly="config.opMode=='edit'" required ng-keyup="config.checkALMProjectKeyAvailability()"
                                        />
                                        <span style='color: red;' ng-model="config.isALMProjectKeyAvailable" ng-show="config.isALMProjectKeyAvailable==false">ALM Project Key already exists!</span>
                                    </div>
                                </div>

                                <hr/>

                                <div class="row">
                                    <label class="control-label col-lg-3 text-right">Folder Name</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" placeholder="Folder Name (Optional)" ng-model="config.tempConfig.JobList.OtherJobs[0].FolderName"
                                        />
                                    </div>
                                </div>

                                <hr/>

                                <div class="row">
                                    <label class="control-label col-lg-3 text-right">Job List</label>

                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" placeholder="Job Name" ng-enter="config.addJob()" ng-model="config.newJob">
                                            </div>
                                            <div class="col-lg-2">
                                                <button type="button" class="btn btn-md btn-success pull-right" ng-click="config.addJob()">
                                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp; Add Job &nbsp;
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br>

                                <div class="row">
                                    <label class="control-label col-lg-3 text-right"></label>

                                    <div class="col-lg-8">
                                        <div class="list-group control-list" style="max-height: 220px;overflow: auto;">
                                            <a href="#" class="list-group-item" id="jobList" ng-repeat="jobName in config.tempConfig.JobList.OtherJobs[0].JobNames track by $index">
                                                <div class="row">
                                                    <div class="col-lg-10">
                                                        <span>{{jobName}}</span>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <button type="button" class="btn btn-default btn-md btn-danger pull-right" ng-click="config.removeJob($index)">
                                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" ng-disabled="(config.opMode=='add' && (configForm.$invalid || !config.isALMProjectKeyAvailable || config.isJobListEmpty)) || (config.opMode=='edit' && config.isJobListEmpty)" class="btn btn-default"
                                    data-dismiss="modal" ng-click="config.save()">Save</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript" src="js/service/jenkins.config.service.js"></script>
<script type="text/javascript" src="js/directive/jenkins.config.directive.js"></script>
<script type="text/javascript" src="js/controller/jenkins.config.controller.js"></script>

</html>