package com.cybage.jenkinsconfig.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class JenkinsWebController {

	private final static Logger LOGGER = Logger.getLogger(JenkinsWebController.class);

	@RequestMapping("/")
	public String showIndexPage(Map<String, Object> model) {
		LOGGER.info("Jenkins Web Controller : PATH : /");
		return "index";
	}

}