/** Copyright Cybage Software To Present
All rights reserved.
*/
package com.cybage.jenkinsconfig.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cybage.jenkinsconfig.exception.JenkinsException;
import com.cybage.jenkinsconfig.model.JobConfig;
import com.cybage.jenkinsconfig.service.JenkinsService;
import com.cybage.jenkinsconfig.util.JsonUtil;

@RestController
public class JenkinsRestController {

	private final static Logger LOGGER = Logger.getLogger(JenkinsRestController.class);

	@Autowired
	JenkinsService jenkinsService;

	@CrossOrigin(origins = "*")
	@GetMapping(value = "/rest/config", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getAllJobsConfig(HttpServletRequest request) throws JenkinsException, Exception {
		LOGGER.info("Method - GET ; PATH - /rest/config");
		return new ResponseEntity<>(JsonUtil.getJsonFromModel(this.jenkinsService.getAllJobsConfig()), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(value = "/rest/config/keys", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getAllALMProjectKeys(HttpServletRequest request) throws JenkinsException, Exception {
		LOGGER.info("Method - GET ; PATH - /rest/config/keys");
		return new ResponseEntity<>(JsonUtil.getJsonFromModel(this.jenkinsService.getAllALMProjectKeys()), HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@GetMapping(value = "/rest/config/{aLMProjectKey}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getJobConfig(@PathVariable String aLMProjectKey, HttpServletRequest request)
			throws JenkinsException, Exception {
		LOGGER.info("Method - GET ; PATH - /rest/config/{aLMProjectKey}");
		return new ResponseEntity<>(JsonUtil.getJsonFromModel(this.jenkinsService.getJobConfig(aLMProjectKey)), HttpStatus.OK);
    }
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/rest/config", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> addJobConfig(@RequestBody String jobConfigStr, HttpServletRequest request)
			throws JenkinsException, Exception {
		JobConfig jobConfig = (JobConfig) JsonUtil.getFromJson(jobConfigStr, JobConfig.class);
		LOGGER.info("Method - POST ; PATH - /rest/config");
		LOGGER.info("JobConfig : " + jobConfig);
		return new ResponseEntity<>(JsonUtil.getJsonFromModel(this.jenkinsService.addJobConfig(jobConfig)), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PutMapping(value = "/rest/config/{aLMProjectKey}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> updateJobConfig(@RequestBody String jobConfigStr, HttpServletRequest request)
			throws JenkinsException, Exception {
		LOGGER.info("Method - PUT ; PATH - /rest/config");
		JobConfig jobConfig = (JobConfig) JsonUtil.getFromJson(jobConfigStr, JobConfig.class);
		return new ResponseEntity<>(JsonUtil.getJsonFromModel(this.jenkinsService.updateJobConfig(jobConfig)), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@DeleteMapping(value = "/rest/config/{aLMProjectKey}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> deleteJobConfig(@PathVariable String aLMProjectKey, HttpServletRequest request)
			throws JenkinsException, Exception {
		LOGGER.info("Method - DELETE ; PATH - /rest/config/{aLMProjectKey");
		return new ResponseEntity<>(JsonUtil.getJsonFromModel(this.jenkinsService.deleteJobConfig(aLMProjectKey)), HttpStatus.OK);
	}

}