/** Copyright Cybage Software To Present
All rights reserved.
*/
package com.cybage.jenkinsconfig.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cybage.jenkinsconfig.exception.JenkinsException;
import com.cybage.jenkinsconfig.model.JobConfig;
import com.cybage.jenkinsconfig.util.JsonUtil;
import com.google.gson.JsonParseException;

@Service
public class JenkinsService {

	private final static Logger LOGGER = Logger.getLogger(JenkinsService.class);

	@Value("${config.file}")
	private String configFile;

	public ArrayList<JobConfig> getAllJobsConfig() {
		File f = new File(configFile);

		try {
			if (f.exists() && !f.isDirectory()) {
				ArrayList<JobConfig> allJobsConfig = new ArrayList<>();
				Collections.addAll(allJobsConfig,
						(JobConfig[]) JsonUtil.getFromJsonArrayFile(new FileReader(f), JobConfig[].class));
				return allJobsConfig;
			} else {
				f.createNewFile();
				ArrayList<JobConfig> allJobsConfig = new ArrayList<>();
				FileWriter fw = new FileWriter(configFile);
				fw.write(JsonUtil.getJsonFromModel(allJobsConfig).toString());
				fw.close();
				return allJobsConfig;
			}
		} catch (JsonParseException e) {
			LOGGER.error("Ivalid JSON in " + configFile + " file");
			throw new JenkinsException(500, "Ivalid JSON in " + configFile + " file");
		} catch (FileNotFoundException e) {
			LOGGER.error("Ivalid JSON in " + configFile + " file");
			throw new JenkinsException(500, "File does not exist : " + configFile);
		} catch (Exception e) {
			throw new JenkinsException(500, e.getLocalizedMessage());
		}
		// throw new JenkinsException(500, "File does not exist : " + configFileName);
	}

	public ArrayList<String> getAllALMProjectKeys() {
		File f = new File(configFile);

		try {
			if (f.exists() && !f.isDirectory()) {
				ArrayList<JobConfig> allJobsConfig = new ArrayList<>();
				Collections.addAll(allJobsConfig,
						(JobConfig[]) JsonUtil.getFromJsonArrayFile(new FileReader(f), JobConfig[].class));
				return (ArrayList<String>) allJobsConfig.stream().map(JobConfig::getALMProjectKey)
						.collect(Collectors.toList());

			} else {
				f.createNewFile();
				ArrayList<String> allALMProjectKeys = new ArrayList<>();
				FileWriter fw = new FileWriter(configFile);
				fw.write(JsonUtil.getJsonFromModel(allALMProjectKeys).toString());
				fw.close();
				return allALMProjectKeys;
			}
		} catch (JsonParseException e) {
			LOGGER.error("Ivalid JSON in " + configFile + " file");
			throw new JenkinsException(500, "Ivalid JSON in " + configFile + " file");
		} catch (FileNotFoundException e) {
			LOGGER.error("Ivalid JSON in " + configFile + " file");
			throw new JenkinsException(500, "File does not exist : " + configFile);
		} catch (Exception e) {
			throw new JenkinsException(500, e.getLocalizedMessage());
		}
		// throw new JenkinsException(500, "File does not exist : " + configFileName);
	}

	public JobConfig getJobConfig(String aLMProjectKey) {
		File f = new File(configFile);

		try {
			if (f.exists() && !f.isDirectory()) {
				ArrayList<JobConfig> allJobsConfig = new ArrayList<>();
				Collections.addAll(allJobsConfig,
						(JobConfig[]) JsonUtil.getFromJsonArrayFile(new FileReader(f), JobConfig[].class));
				return allJobsConfig.stream().filter(j -> j.getALMProjectKey().equals(aLMProjectKey)).findFirst().get();
			}
		} catch (JsonParseException e) {
			LOGGER.error("Ivalid JSON in " + configFile + " file");
			throw new JenkinsException(500, "Ivalid JSON in " + configFile + " file");
		} catch (FileNotFoundException e) {
			LOGGER.error("Ivalid JSON in " + configFile + " file");
			throw new JenkinsException(500, "File does not exist : " + configFile);
		} catch (Exception e) {
			throw new JenkinsException(500, e.getLocalizedMessage());
		}
		throw new JenkinsException(500, "File does not exist : " + configFile);
	}

	public JobConfig updateJobConfig(JobConfig jobConfig) {
		File f = new File(configFile);

		try {
			if (f.exists() && !f.isDirectory()) {
				ArrayList<JobConfig> allJobsConfig = new ArrayList<>();
				Collections.addAll(allJobsConfig,
						(JobConfig[]) JsonUtil.getFromJsonArrayFile(new FileReader(f), JobConfig[].class));
				allJobsConfig.remove(allJobsConfig.stream()
						.filter(j -> j.getALMProjectKey().equals(jobConfig.getALMProjectKey())).findFirst().get());
				allJobsConfig.add(jobConfig);
				f.delete();
				FileWriter fw = new FileWriter(configFile);
				fw.write(JsonUtil.getJsonFromModel(allJobsConfig).toString());
				fw.close();
				return jobConfig;
			} else {
				throw new JenkinsException(500, "File does not exist : " + configFile);
			}
		} catch (JsonParseException e) {
			LOGGER.error("Ivalid JSON in " + configFile + " file");
			throw new JenkinsException(500, "Ivalid JSON in " + configFile + " file");
		} catch (FileNotFoundException e) {
			LOGGER.error("Ivalid JSON in " + configFile + " file");
			throw new JenkinsException(500, "File does not exist : " + configFile);
		} catch (Exception e) {
			throw new JenkinsException(500, e.getLocalizedMessage());
		}
	}

	public JobConfig addJobConfig(JobConfig jobConfig) {
		File f = new File(configFile);

		try {
			if (f.exists() && !f.isDirectory()) {
				ArrayList<JobConfig> allJobsConfig = new ArrayList<>();

				Collections.addAll(allJobsConfig,
						(JobConfig[]) JsonUtil.getFromJsonArrayFile(new FileReader(f), JobConfig[].class));

				allJobsConfig.add(jobConfig);
				f.delete();
				FileWriter fw = new FileWriter(configFile);
				fw.write(JsonUtil.getJsonFromModel(allJobsConfig).toString());
				fw.close();
				return jobConfig;
			} else {
				// throw new JenkinsException(500, "File does not exist : " + configFileName);
				ArrayList<JobConfig> allJobsConfig = new ArrayList<>();
				allJobsConfig.add(jobConfig);
				FileWriter fw = new FileWriter(configFile);
				fw.write(JsonUtil.getJsonFromModel(allJobsConfig).toString());
				fw.close();
				return jobConfig;
			}
		} catch (JsonParseException e) {
			LOGGER.error("Ivalid JSON in " + configFile + " file");
			throw new JenkinsException(500, "Ivalid JSON in " + configFile + " file");
		} catch (FileNotFoundException e) {
			LOGGER.error("Ivalid JSON in " + configFile + " file");
			throw new JenkinsException(500, "File does not exist : " + configFile);
		} catch (Exception e) {
			e.printStackTrace();
			throw new JenkinsException(500, e.getLocalizedMessage());
		}
	}

	public JobConfig deleteJobConfig(String aLMProjectKey) {
		File f = new File(configFile);

		try {
			if (f.exists() && !f.isDirectory()) {
				ArrayList<JobConfig> allJobsConfig = new ArrayList<>();
				Collections.addAll(allJobsConfig,
						(JobConfig[]) JsonUtil.getFromJsonArrayFile(new FileReader(f), JobConfig[].class));
				JobConfig jobConfig = allJobsConfig.stream().filter(j -> j.getALMProjectKey().equals(aLMProjectKey))
						.findFirst().get();
				allJobsConfig.remove(allJobsConfig.stream().filter(j -> j.getALMProjectKey().equals(aLMProjectKey))
						.findFirst().get());
				f.delete();
				FileWriter fw = new FileWriter(configFile);
				fw.write(JsonUtil.getJsonFromModel(allJobsConfig).toString());
				fw.close();
				return jobConfig;
			} else {
				throw new JenkinsException(500, "File does not exist : " + configFile);
			}
		} catch (JsonParseException e) {
			LOGGER.error("Ivalid JSON in " + configFile + " file");
			throw new JenkinsException(500, "Ivalid JSON in " + configFile + " file");
		} catch (FileNotFoundException e) {
			LOGGER.error("Ivalid JSON in " + configFile + " file");
			throw new JenkinsException(500, "File does not exist : " + configFile);
		} catch (Exception e) {
			throw new JenkinsException(500, e.getLocalizedMessage());
		}
	}

}
