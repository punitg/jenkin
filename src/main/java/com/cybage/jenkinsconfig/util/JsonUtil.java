package com.cybage.jenkinsconfig.util;

import java.io.FileReader;
import java.lang.reflect.Type;

import com.cybage.jenkinsconfig.exception.JenkinsException;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

public class JsonUtil {

	private static Gson gson;

	static {
		gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).serializeNulls().create();
	}

	public static Object[] getFromJsonArray(String key, Type className) throws JenkinsException {
		try {
			return gson.fromJson(key, className);
		} catch (JsonSyntaxException ex) {
			throw new JenkinsException(500, "Invalid Json");
		}
	}
	
	public static Object[] getFromJsonArrayFile(FileReader key, Type className) throws JenkinsException {
		try {
			return gson.fromJson(key, className);
		} catch (JsonSyntaxException ex) {
			throw new JenkinsException(500, "Invalid Json");
		}
	}

	public static Object getFromJson(String key, Type className) throws JenkinsException {
		try {
			return gson.fromJson(key, className);
		} catch (JsonSyntaxException ex) {
			throw new JenkinsException(500, "Invalid Json");
		}
	}

	public static Object getJsonFromModel(Object object) {
		return gson.toJson(object);
	}
}