package com.cybage.jenkinsconfig.exception;

public class JenkinsExceptionResponse {
	private int status;
	private String message;

	public JenkinsExceptionResponse(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public JenkinsExceptionResponse() {
		super();
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "JenkinsExceptionResponse [status=" + status + ", message="
				+ message + "]";
	}

}
