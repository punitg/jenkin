package com.cybage.jenkinsconfig.exception;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JenkinsException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	@SerializedName("status")
	@Expose
	private int status;
	@SerializedName("message")
	@Expose
	private String message;

	public JenkinsException(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "JenkinsException [status=" + status + ", message=" + message + "]";
	}

}
