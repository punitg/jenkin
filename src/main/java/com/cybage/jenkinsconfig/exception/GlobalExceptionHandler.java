package com.cybage.jenkinsconfig.exception;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	private final Logger LOGGER = Logger.getLogger(GlobalExceptionHandler.class);

	public GlobalExceptionHandler() {
		super();
	}

	@ExceptionHandler({ JenkinsException.class })
	public ResponseEntity<JenkinsExceptionResponse> handleException(JenkinsException ex, WebRequest request) {
		JenkinsExceptionResponse jenkinsExceptionResponse = new JenkinsExceptionResponse();
		jenkinsExceptionResponse.setMessage(ex.getMessage());
		jenkinsExceptionResponse.setStatus(ex.getStatus());
		this.LOGGER.error("JenkinsExceptionResponse : " + jenkinsExceptionResponse);
		return new ResponseEntity<>(jenkinsExceptionResponse, HttpStatus.valueOf(jenkinsExceptionResponse.getStatus()));
	}

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<JenkinsExceptionResponse> handleException(Exception ex) {
		JenkinsExceptionResponse jenkinsExceptionResponse = new JenkinsExceptionResponse();
		jenkinsExceptionResponse.setMessage(ex.getMessage());
		jenkinsExceptionResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		this.LOGGER.error("JenkinsExceptionResponse : " + jenkinsExceptionResponse);
		return new ResponseEntity<>(jenkinsExceptionResponse, HttpStatus.valueOf(jenkinsExceptionResponse.getStatus()));
	}
}