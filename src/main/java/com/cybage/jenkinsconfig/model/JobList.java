
package com.cybage.jenkinsconfig.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * JobList model stores information of CI and Non-CI jobs read from jenkins-config.json file.
 * 
 * @version 1.0
 */
public class JobList {

	@SerializedName("OtherJobs")
	@Expose
	private List<Jobs> otherJobs;
	@SerializedName("CIJobs")
	@Expose
	private List<Jobs> cIJobs;

	public List<Jobs> getOtherJobs() {
		return new ArrayList<>(this.otherJobs);
	}

	public void setOtherJobs(List<Jobs> otherJobs) {
		this.otherJobs = new ArrayList<>(otherJobs);
	}

	public List<Jobs> getCIJobs() {
		return new ArrayList<>(this.cIJobs);
	}

	public void setCIJobs(List<Jobs> cIJobs) {
		this.cIJobs = new ArrayList<>(cIJobs);
	}

	@Override
	public String toString() {
		return "JobList [otherjobs=" + this.otherJobs + ", cIJobs=" + this.cIJobs + "]";
	}

}
