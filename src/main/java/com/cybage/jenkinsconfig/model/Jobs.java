
package com.cybage.jenkinsconfig.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Jobs model stores folder and job names related information read from jenkins-config.json file.
 * 
 * @version 1.0
 */
public class Jobs {

	@SerializedName("FolderName")
	@Expose
	private String folderName;
	@SerializedName("JobNames")
	@Expose
	private List<String> jobNames;

	public String getFolderName() {
		return this.folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public List<String> getJobNames() {
		return new ArrayList<>(this.jobNames);
	}

	public void setJobNames(List<String> jobNames) {
		this.jobNames = new ArrayList<>(jobNames);
	}

	@Override
	public String toString() {
		return "Jobs [folderName=" + this.folderName + ", jobNames=" + this.jobNames + "]";
	}

}
