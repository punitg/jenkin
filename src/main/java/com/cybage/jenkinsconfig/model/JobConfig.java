
package com.cybage.jenkinsconfig.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * JobConfig model stores configuration information read from jenkins-config.json file.
 * 
 * @version 1.0
 */
public class JobConfig {

	@SerializedName("ALMProjectKey")
	@Expose
	private String aLMProjectKey;
	@SerializedName("JobList")
	@Expose
	private JobList jobList;

	public String getALMProjectKey() {
		return this.aLMProjectKey;
	}

	public void setALMProjectKey(String aLMProjectKey) {
		this.aLMProjectKey = aLMProjectKey;
	}

	public JobList getJobList() {
		return this.jobList;
	}

	public void setJobList(JobList jobList) {
		this.jobList = jobList;
	}

	@Override
	public String toString() {
		return "JobConfig [aLMProjectKey=" + this.aLMProjectKey + ", jobList=" + this.jobList + "]";
	}

}
